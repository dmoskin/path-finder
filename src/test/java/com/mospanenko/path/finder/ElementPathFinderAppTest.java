package com.mospanenko.path.finder;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by dmitri on 16.12.18.
 */
public class ElementPathFinderAppTest {

    String resourcePath = "./samples/SB Admin 2 - Bootstrap Admin Theme original.html";
    String testPath = "./samples/SB Admin 2 - Bootstrap Admin Theme.html";
    String testPath1 = "./samples/SB Admin 2 - Bootstrap Admin Theme 1.html";
    String testPath2 = "./samples/SB Admin 2 - Bootstrap Admin Theme2.html";
    String testPath3 = "./samples/SB Admin 2 - Bootstrap Admin Theme3.html";
    String targetElementId = "make-everything-ok-button";

    
    @Test
    public void test() {
        ElementPathFinderApp.findSimilar(resourcePath, testPath, targetElementId);
        ElementPathFinderApp.findSimilar(resourcePath, testPath1, targetElementId);
        ElementPathFinderApp.findSimilar(resourcePath, testPath2, targetElementId);
        ElementPathFinderApp.findSimilar(resourcePath, testPath3, targetElementId);
    }

}