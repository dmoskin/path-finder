package com.mospanenko.path.finder;

import com.mospanenko.path.finder.api.ControlElementCriteria;
import com.mospanenko.path.finder.api.PathFinder;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by dmitri on 16.12.18.
 */
public class PathFinderImpl implements PathFinder {
    private static final Logger LOG = LoggerFactory.getLogger(PathFinderImpl.class);
    private Document document;
    private Element similarElement;

    private static Map<Element, Integer> similarityCount = new HashMap<>();

    @Override
    public void findSimilarElement(String testFilePath, ControlElementCriteria criteria) {
        DocumentWorker.parseFile(testFilePath).ifPresent(parcedDoc -> document = parcedDoc);

        for (String aCriteria : criteria) {
            Elements elements = document.select(aCriteria);
            elements.forEach(e -> {
                if(similarityCount.get(e) == null && e.text().contains(criteria.getContentCriteria())) {
                    similarityCount.merge(e, 1, Integer::sum);
                }
                similarityCount.merge(e, 1, Integer::sum);
            });
        }

        Map.Entry<Element, Integer> maxSimilarElement = similarityCount.entrySet().stream()
                .max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1)
                .get();

        LOG.info("Found element with {} matches", maxSimilarElement.getValue());
        similarElement = maxSimilarElement.getKey();

        showSimilarElement();
        similarityCount.clear();
    }

    private void showSimilarElement() {
        StringBuilder b = new StringBuilder();
        Elements parents = similarElement.parents();
        new LinkedList<>(parents)
                .descendingIterator()
                .forEachRemaining(parer -> b.append(parer.tag() + "->"));
        b.append(similarElement.tag());
        LOG.info("Element path: {}", b);
    }


}
