package com.mospanenko.path.finder;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

/**
 * Created by dmitri on 16.12.18.
 */
public class DocumentWorker {
    private static final Logger LOG  = LoggerFactory.getLogger(DocumentWorker.class);
    private static String CHARSET_NAME = "utf8";


    public static Optional<Document> parseFile(String filePath) {
        File controlFile = new File(filePath);
        if(controlFile.exists()) {
            try {
                return Optional.ofNullable(Jsoup.parse(
                        controlFile,
                        CHARSET_NAME,
                        controlFile.getAbsolutePath()));

            } catch (IOException e) {
                LOG.error("Error reading [{}] file", controlFile.getAbsolutePath(), e);
                return Optional.empty();
            }

        } else {
            LOG.error("File does not exists in path {}", filePath);
            return Optional.empty();
        }
    }
}
