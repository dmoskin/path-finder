package com.mospanenko.path.finder;

import com.mospanenko.path.finder.api.ControlElementCriteria;

import java.util.*;

/**
 * Created by dmitri on 16.12.18.
 */
public class ControlElementCriteriaImpl implements ControlElementCriteria {

    private List<CriteriaEntry> criteriaEntryList = new ArrayList<>();
    private String content;

    @Override
    public void addCriteria(String key, String value) {
        CriteriaEntry entry = new CriteriaEntry(key, value);
        criteriaEntryList.add(entry);
    }

    @Override
    public void addContentCriteria(String text) {
        content = text;
    }

    @Override
    public String getContentCriteria() {
        return content;
    }


    @Override
    public int criteriaSize() {
        return criteriaEntryList.size();
    }

    @Override
    public Iterator<String> iterator() {
        return new Iterator<String>() {
            private Iterator it = criteriaEntryList.iterator();
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }

            @Override
            public String next() {
                if(hasNext()) {
                    CriteriaEntry entry = (CriteriaEntry) it.next();
                    return String.format("[%s*=%s]", entry.getKey(), entry.getAttr());
                } else {
                    throw new NoSuchElementException("");
                }

            }
        };
    }



    private static class CriteriaEntry {
        private String key;
        private String attr;

        public CriteriaEntry(String key, String attr) {
            this.key = key;
            this.attr = attr;
        }

        public String getKey() {
            return key;
        }

        public String getAttr() {
            return attr;
        }
    }
}
