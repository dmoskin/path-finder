package com.mospanenko.path.finder;

import com.mospanenko.path.finder.api.ControlElementCriteria;
import com.mospanenko.path.finder.api.PathFinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by dmitri on 16.12.18.
 */
public final class ElementPathFinderApp {
    private static final Logger LOG  = LoggerFactory.getLogger(ElementPathFinderApp.class);

    private static final int MINIMUM_ARG_NUMBER = 2;
    private static final int CONTROL_SAMPLE_INDEX = 0;
    private static final int TEST_SAMPLE_INDEX = 1;
    private static final int ID_ELEMENT_TO_FIND_INDEX = 2;
    private static final String DEFAULT_ELEMNT_ID = "make-everything-ok-button";
    private static String idElementToFind;

    public static void main(String[] args) {
        if(args.length < MINIMUM_ARG_NUMBER) {
            LOG.info("Provide minimum two arguments with file path to process.");
        } else {
            String originalFile = args[CONTROL_SAMPLE_INDEX];
            String compareFile = args[TEST_SAMPLE_INDEX];

            if(args.length > MINIMUM_ARG_NUMBER) {
                idElementToFind = args[ID_ELEMENT_TO_FIND_INDEX];
            } else {
                idElementToFind = DEFAULT_ELEMNT_ID;
            }
            LOG.info("Target {}", originalFile);
            LOG.info("Test {}", compareFile);

            findSimilar(originalFile, compareFile, idElementToFind);
        }
    }

    public static void findSimilar(String controlPath, String testPath, String elementId) {
        ControlElementCriteria criteria = CriteriaBuilder.buildCriteria(controlPath, elementId);
        PathFinder finder = new PathFinderImpl();
        finder.findSimilarElement(testPath, criteria);
    }
}
