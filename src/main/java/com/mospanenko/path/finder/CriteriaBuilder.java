package com.mospanenko.path.finder;

import com.mospanenko.path.finder.api.ControlElementCriteria;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.stream.StreamSupport;

/**
 * Created by dmitri on 16.12.18.
 */
public final class CriteriaBuilder extends DocumentWorker {
    private static final Logger LOG  = LoggerFactory.getLogger(CriteriaBuilder.class);
    private static String CHARSET_NAME = "utf8";


    private CriteriaBuilder() {}


    public static ControlElementCriteria buildCriteria(String filePath , String elementId) {
        ControlElementCriteria criteria = new ControlElementCriteriaImpl();
        findElementForCriteria(filePath, elementId)
                .ifPresent(element -> {
                    criteria.addContentCriteria(element.text());

                    StreamSupport.stream(element.attributes().spliterator(), false)
                            .forEach(attribute -> {
                                String[] split = attribute.getValue().split("\\s");
                                for (String attrText : split) {
                                    criteria.addCriteria(attribute.getKey(), attrText);
                                }
                            });
                });

        LOG.info("Found {} criteria to check", criteria.criteriaSize());
        return criteria;
    }


    private static Optional<Element> findElementForCriteria(String pathToControlFile, String elementId) {
        Optional<Document> document = DocumentWorker.parseFile(pathToControlFile);
        if(document.isPresent()) {
            return Optional.of(document.get().getElementById(elementId));
        } else {
            return Optional.empty();
        }

    }
}
