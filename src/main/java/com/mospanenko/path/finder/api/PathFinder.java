package com.mospanenko.path.finder.api;

/**
 * Created by dmitri on 16.12.18.
 */
public interface PathFinder {
    void findSimilarElement(String testFilePath, ControlElementCriteria criteria);
}
