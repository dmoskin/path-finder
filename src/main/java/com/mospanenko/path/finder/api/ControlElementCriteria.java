package com.mospanenko.path.finder.api;

import java.util.Iterator;

/**
 * Created by dmitri on 16.12.18.
 */
public interface ControlElementCriteria extends Iterable<String>{

    void addCriteria(String key, String value);
    void addContentCriteria(String text);
    String getContentCriteria();
    int criteriaSize();
}
