Build project.

`gradlew shadowJar`

for building fatJar with dependencies.

Binary version provided in samples directory.

`/path-finder/samples/pathfinder-all.jar`

Running application
'java -jar <pathToJar>pathfinder-all.jar <pathToControlFile> <pathToTestFile> <ElementIdToSearch>'

<ElementIdToSearch> - is optional, default value is "make-everything-ok-button"